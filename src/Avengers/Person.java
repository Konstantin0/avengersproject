package Avengers;

public class Person extends Address{

    private String name;
    private int age;

    public Person(String addres, int index, String name, int age) {
        super(addres, index);
        this.name = name;
        this.age = age;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
