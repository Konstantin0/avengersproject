package Avengers;

public class Student extends Person{

    private double averageScore;//Средний бал
    private boolean studies; //Статус студента

    public Student(String addres, int index, String name, int age, double averageScore, boolean studies) {
        super(addres, index, name, age);
        this.averageScore = averageScore;
        this.studies = studies;
    }


    public boolean isStudies() {
        return studies;
    }

    public void setStudies(boolean studies) {
        this.studies = studies;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        if (averageScore >= 0 && averageScore <= 10 ) {
            this.averageScore = averageScore;
        } else System.out.println("Средний балл должен быть от 0 до 10");
    }


}
