package Avengers;

import java.util.ArrayList;

public class StudentUtil {

//метод, который возвращает массив студентов, которые учатся в институте на текущий момент времени
//(определяем по статусу студента - см. выше)
//массив должен возврашать массив той длины сколько студентов учатся (нужно создать либо динамический массив либо увелисивать и уменьшать длинну массива в зависимости от колва студентов



    public static Student[] currentlyStudying(Student[] array) {


            Student stub = new Student("Студент отчислен ", 0, "Студент отчислен ", 0, 0, true); //заглушка

            Student[] result = new Student[array.length];
            for (int i = 0; i < array.length; i++) {
                if (array[i].isStudies() == true)
                    result[i] = array[i];
                else
                    result[i] = stub; //идти по массиву result и если значение = null удалить ячейку


                //System.out.println("Студенты которые учатся в институте: " +  result[i].getName());
            }
            return result;
        }

//метод для формирования отчета в виде строки символов о текущей успеваемости студентов.
//Метод должен возвращать значение типа String.
        public static String createProgressReport (Student[]array){
            String result = "Студенты которые учатся в институте \n";
            for (int i = 0; i < array.length; i++) {
                result += "Имя студента: " + array[i].getName() + ". Средний балл: " + array[i].getAverageScore() + "\n";

            }
            return result;

        }

    }



